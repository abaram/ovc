% sets paths
% populates the 'is' parameter structure
% calls SepcifySubjects_AB.m

is = struct;

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% PATHS 
%--------------------------------------------------------------------------

% toolboxes and analysis scripts 

if exist('/Users/abaram/OneDrive - Nexus365/projects/megOvc','dir') % WASABI (FMRIB Mac)
    osl_path = '/home/fs0/abaram/scratch/MATLAB/osl';    
    is.spm_path = '/home/fs0/abaram/scratch/MATLAB/osl/spm12';
%     is.spm_path = '/Users/abaram/Documents/MATLAB/spm12';
    is.rootMEG =  '/Users/abaram/OneDrive - Nexus365/projects/ovc/pilot1';
elseif exist('/Users/neurotheory/OneDrive - Nexus365/projects/ovc','dir') % laptop
    osl_path = '/Users/neurotheory/Documents/MATLAB/osl';
    is.spm_path = '/Users/neurotheory/Documents/MATLAB/spm12';    
    is.rootMEG =  '/Users/neurotheory/OneDrive - Nexus365/projects/ovc/pilot1';    
else
    error('didnt find root dir')
end
cd(fullfile(osl_path,'osl-core'))
OSLDIR = getenv('OSLDIR');
addpath(OSLDIR);
osl_startup(OSLDIR);
addpath(genpath(based)) % already did this before but starting OSL cleared the path


% data
is.rawMEG =   [is.rootMEG '/raw/'];                     % raw MEG from scanner
is.OPTPath =  [is.rootMEG '/OPTdata/'];                 % savedir for preprocessed data
is.OutputDir = [is.rootMEG '/outputTDLM/'];     % savedir for TDLM
mkdir([is.OutputDir '/trained_decoders_stimId']);
mkdir([is.OutputDir '/trained_decoders_ovc']);
mkdir([is.OutputDir '/trained_decoders_pos']);
% mkdir([is.OutputDir '/decoded_rest']);

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% PRE-PROCESSING PARAMS
%--------------------------------------------------------------------------

is.numChan = 273;           
is.sampleRate = 1200;       % sample rate of raw MEG data (c.f. YL 600 ms) 
is.smoothFact = 12;         % to get 100Hz final s.r., as per YL MFStartup, MMN
is.msPerSample = 1000 * is.smoothFact / is.sampleRate; % 10 ms, ms per sample of preprocessed MEG data (100Hz)
is.highpass = 0.5;          % high pass filter Hz as per YL MFStartup

% training data epoching (prev. functional localizer epoching)
is.sfl_triggers = [1:7];                           % currently these are the position, should be the OVCs. 
is.epoch_win_s = [-0.5 1];                              % epoch win w.r.t. pic onset trigger. similiar to MMN. YL chose [-3.5 1], but this is of no consequence (also he used a different localizer task structure)
is.epoch_win_bins = 1000*is.epoch_win_s/is.msPerSample;
is.picOnsetSlice = -(is.epoch_win_bins(1)) + 1;         % time bin of pic onset in epoched MEG data

% rest session start and end triggers


%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% DECODING AND TDLM ANALYSIS PARAMS
%--------------------------------------------------------------------------
% Decoder training params
is.nullslice = 250;             % nullslice ms, time BEFORE pic onset (note different localizer trial structure between tasks). for MMN it was 500
is.ENL1 = 0.006;                % L1 parameter = 0.006, as per Kurth-Nelson et al 2016
is.eyeBlinkSens = 0;            % 0=do NOT exclude sensors associated with eyeblinks (as per YL)

% decoder train time (MMN train the time of peak decodability)
% is.tss = 18;                    % bins post-pic onset for decoder training (time of peak decodability in CV = 180 ms)
% AB: I couldn't understand why both is.tss and is.whichTimes are needed,
% they seem to be doing the same role (with is.whichTimes just taking a
% subset of the bins in is.tss. So I got rid of is.tss and only use
% is.whichTimes.
% YL : is.whichTimes=[28:47]; % 10th is 37 -> 200 ms
is.whichTimes=-10:60; % bins post-pic onset for decoder training (10:30 is from 100ms to 300 ms)

% Sequencness 
is.lgncy = 1:60;                % lags in units of samples, as per YL MFStartup

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% SUBJECTS
%--------------------------------------------------------------------------
SpecifySubjects_AB

