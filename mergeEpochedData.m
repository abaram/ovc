%% add paths
if exist('/Users/abaram/OneDrive - Nexus365/projects/megOvc','dir') % WASABI (FMRIB Mac)
    root = '/Users/abaram/OneDrive - Nexus365/projects/megOvc/';
    osl_path = '/home/fs0/abaram/scratch/MATLAB/osl';    
elseif exist('/Users/neurotheory/OneDrive - Nexus365/projects/megOvc','dir') % laptop
    root = '/Users/neurotheory/OneDrive - Nexus365/projects/megOvc';
    osl_path = '/Users/neurotheory/Documents/MATLAB/osl';
else
    error('didnt find root dir')
end
addpath(osl_path);
osl_core_path   = fullfile(osl_path,'osl-core');
addpath(genpath(osl_core_path))
% osl_startup(osl_path);

data_path = fullfile(root,'pilotBeijing');
script_path = fullfile(root,'analysis');
addpath(genpath(script_path));

cd(root)

% get all S??_*.ds folders and put their names in a cell array
subject = dir(fullfile(data_path,'S*'));
subject = {subject.name};

highpassCutoff = 0.5;
toMerge = [];
for iSubj = 1:length(subject)
    fprintf('------------Doing subject %s ------------\n',(subject{iSubj}))
    sub = subject{iSubj};
    subjDataDir = fullfile(data_path,sub);
    subjOutputDir  = fullfile(root,'derivatives','preproc',sub);
    cd(subjOutputDir)
    runsDirsNames = dir(fullfile(subjDataDir,[sub '_G31BNU_*.ds'])); % only task phase        
    % cell array of epoched MEEG objects to merge
    for iRun=1:length(runsDirsNames)
        fName = strcat(fullfile(root,'derivatives','preproc',sub),'/',runsDirsNames(iRun).name,'/',['highpass_' num2str(highpassCutoff) '.opt'],'/','ReABdffspmeeg.mat');
        toMerge.D{iRun} = spm_eeg_load(fName);
    end
    D = spm_eeg_merge(toMerge);
    
    % plot trials
    figure('units','normalized','outerposition',[0 0 0.6 0.3]);
    imagesc(D.time,[],squeeze(mean(D(:,:,:),3)));
    xlabel('Time (seconds)','FontSize',20);
    ylabel('Sensors','FontSize',20);colorbar
    title('ERF','FontSize',20)
    set(gca,'FontSize',20)
    set(gca,'XLim',[-0.5 1])
end