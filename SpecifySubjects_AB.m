    % subject list to preprocesess

is.nSubj = 1;


%% [1]
is.fnSID_orig{is.nSubj} = 'S01';
is.fnSID_bids{is.nSubj} = 'sub-01';
is.MEGruns_orig{is.nSubj} = {'S01_G31BNU_20220403_01' 'S01_G31BNU_20220403_02' 'S01-resting_G31BNU_20220403_01' ...
    'S01_G31BNU_20220403_03' 'S01_G31BNU_20220403_04' 'S01-resting_G31BNU_20220403_02' ...
    'S01_G31BNU_20220403_05' 'S01_G31BNU_20220403_06' 'S01-resting_G31BNU_20220403_03' ...
    'S01_G31BNU_20220403_07' 'S01_G31BNU_20220403_08' 'S01-resting_G31BNU_20220403_04' ...
    'S01_G31BNU_20220403_09' 'S01_G31BNU_20220403_10' 'S01-resting_G31BNU_20220403_05' ...
    'S01_G31BNU_20220403_11' 'S01_G31BNU_20220403_12' 'S01-resting_G31BNU_20220403_06' ...
    'S01_G31BNU_20220403_13' 'S01_G31BNU_20220403_14' 'S01-resting_G31BNU_20220403_07'};
is.MEGruns_bids{is.nSubj} = {'sub-01_task-ovc_run-01' 'sub-01_task-ovc_run-02' 'sub-01_task-rest_run-01' ...
    'sub-01_task-ovc_run-03' 'sub-01_task-ovc_run-04' 'sub-01_task-rest_run-02' ...
    'sub-01_task-ovc_run-05' 'sub-01_task-ovc_run-06' 'sub-01_task-rest_run-03' ...
    'sub-01_task-ovc_run-07' 'sub-01_task-ovc_run-08' 'sub-01_task-rest_run-04' ...
    'sub-01_task-ovc_run-09' 'sub-01_task-ovc_run-10' 'sub-01_task-rest_run-05' ...
    'sub-01_task-ovc_run-11' 'sub-01_task-ovc_run-12' 'sub-01_task-rest_run-06' ...
    'sub-01_task-ovc_run-13' 'sub-01_task-ovc_run-14' 'sub-01_task-rest_run-07' };
% increment
is.nSubj = is.nSubj + 1;


%% [2]
is.fnSID_orig{is.nSubj} = 'S04';
is.fnSID_bids{is.nSubj} = 'sub-04';
is.MEGruns_orig{is.nSubj} = {'S04_G31BNU_20220423_01' 'S04_G31BNU_20220423_02' 'S04-resting_G31BNU_20220423_01' ...
    'S04_G31BNU_20220423_03' 'S04_G31BNU_20220423_04' 'S04-resting_G31BNU_20220423_02' ...
    'S04_G31BNU_20220423_05' 'S04_G31BNU_20220423_06' 'S04-resting_G31BNU_20220423_03' ...
    'S04_G31BNU_20220423_07' 'S04_G31BNU_20220423_08' 'S04-resting_G31BNU_20220423_04' ...
    'S04_G31BNU_20220423_09' 'S04_G31BNU_20220423_10' 'S04-resting_G31BNU_20220423_05' ...
    'S04_G31BNU_20220423_11' 'S04_G31BNU_20220423_12' 'S04-resting_G31BNU_20220423_06' ...
    'S04_G31BNU_20220423_13' 'S04_G31BNU_20220423_14' 'S04-resting_G31BNU_20220423_07' ....
    'S04_G31BNU_20220423_15' 'S04_G31BNU_20220423_16' 'S04-resting_G31BNU_20220423_08'};
is.MEGruns_bids{is.nSubj} = {'sub-04_task-ovc_run-01' 'sub-04_task-ovc_run-02' 'sub-04_task-rest_run-01' ...
    'sub-04_task-ovc_run-03' 'sub-04_task-ovc_run-04' 'sub-04_task-rest_run-02' ...
    'sub-04_task-ovc_run-05' 'sub-04_task-ovc_run-06' 'sub-04_task-rest_run-03' ...
    'sub-04_task-ovc_run-07' 'sub-04_task-ovc_run-08' 'sub-04_task-rest_run-04' ...
    'sub-04_task-ovc_run-09' 'sub-04_task-ovc_run-10' 'sub-04_task-rest_run-05' ...
    'sub-04_task-ovc_run-11' 'sub-04_task-ovc_run-12' 'sub-04_task-rest_run-06' ...
    'sub-04_task-ovc_run-13' 'sub-04_task-ovc_run-14' 'sub-04_task-rest_run-07' ...
    'sub-04_task-ovc_run-15' 'sub-04_task-ovc_run-16' 'sub-04_task-rest_run-08' };

% no increment for final subj

fprintf('number of subjects: %d \n',is.nSubj)



