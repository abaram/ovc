% Preproc of MEG data
% AB 24/05/2022 based on Philipp's and Yunzhe's code
clear


%% add paths
if exist('/Users/abaram/OneDrive - Nexus365/projects/megOvc','dir') % WASABI (FMRIB Mac)
    root = '/Users/abaram/OneDrive - Nexus365/projects/megOvc/';
    osl_path = '/home/fs0/abaram/scratch/MATLAB/osl';    
elseif exist('/Users/neurotheory/OneDrive - Nexus365/projects/megOvc','dir') % laptop
    root = '/Users/neurotheory/OneDrive - Nexus365/projects/megOvc';
    osl_path = '/Users/neurotheory/Documents/MATLAB/osl';
else
    error('didnt find root dir')
end
data_path = fullfile(root,'pilotBeijing');
script_path = fullfile(root,'analysis');
behav_result_path = fullfile(data_path,'behData');

% osl_path = getenv('OSLDIR');
addpath(osl_path);
osl_core_path   = fullfile(osl_path,'osl-core');
addpath(genpath(osl_core_path))

osl_startup(osl_path);
addpath(genpath(script_path));

cd(root)

%%
% get all S??_*.ds folders and put their names in a cell array
subject = dir(fullfile(data_path,'S*'));
subject = {subject.name};

highpassCutoff = 0.5;
sampleRate = 1200;
downsample = 400;

do_import = true;
do_highp  = true;
do_downs  = true;
do_ica    = true;
do_epoch  = true;
do_anyway = false;

task_phase     = true; %
rest_phase     = false;


%%

for iSubj = 1:length(subject)
    fprintf('------------Doing subject %s ------------\n',(subject{iSubj}))
    sub = subject{iSubj};
    subjDataDir = fullfile(data_path,sub);
    if task_phase
        runsDirsNames = dir(fullfile(subjDataDir,[sub '_G31BNU_*.ds']));
    elseif rest_phase
        runsDirsNames = dir(fullfile(subjDataDir,[sub '-resting_G31BNU_*.ds']));
    end
    
    runsDirsNames = {runsDirsNames.name};
    subjOutputDir  = fullfile(root,'derivatives','preproc',sub);

    for iRun = 1:length(runsDirsNames)
        % path to raw data dir for current run
        dataDir = fullfile(subjDataDir,runsDirsNames{iRun});
        % path to dir with results of preprocessing. Currently including
        % the .ds extention. 
        % (this is a combination of both the OPT and spm_files folders in
        % the OPT tutorial)
        outputDir = fullfile(subjOutputDir,runsDirsNames{iRun});
        if ~exist(outputDir,'dir')
            mkdir(outputDir);
        end
        
        %% Import
        
        if do_import
            
            S2 = [];
            
            S2.outfile = fullfile(outputDir,'spmeeg.mat');
            S2.other_channels = {'UADC001','UADC002','UADC003'};
            D = osl_import(dataDir,S2); % this will save the files spmeeg.mat and spmeeg.dat
            
            %% add up other channels
%             D = D.chantype(find(strcmp(D.chanlabels,'UADC001')),'EOG1');
%             D = D.chantype(find(strcmp(D.chanlabels,'UADC002')),'EOG2');
%             D = D.chantype(find(strcmp(D.chanlabels,'UADC003')),'EOG3');
%             
            D.save()
%             
            
            %% Crop unused data - IMPORTANT!!!
            S = struct;
            S.D = fullfile(outputDir,'spmeeg.mat');
            S.prefix='';
            event = ft_read_event(dataDir);
            sampleLastEvent = [event(length(event)).sample]';
            winPostLastEvent = 500; % how long in ms to retain after the last trial trigger
            S.timewin = [0,ceil(sampleLastEvent/sampleRate*1000) + winPostLastEvent];
            
            D=spm_eeg_crop_YL(S);
            D.save()
            
        end
        fprintf('data import done\n')
        
        %% Phase 1 - Filter
        if do_highp
            
            opt=[];
            %             opt.maxfilter.do=0; % will be set to 0 based on data type
            
            spm_files{1}=fullfile(outputDir,'spmeeg.mat');
            structural_files{1}=[]; % leave empty if no .nii structural file available
            
            opt.spm_files=spm_files;
            opt.datatype='ctf';
            
            % HIGHPASS
            if do_highp ==0
                opt.highpass.do=0;
                opt.dirname=fullfile(outputDir,'highpass_0');
            else
                opt.highpass.cutoff = highpassCutoff; % create different folder for different frenquency band (0.5; 1; 20)
                opt.dirname=fullfile(outputDir,['highpass_',num2str(highpassCutoff)]);
                opt.highpass.do=1;
            end
            
            % Notch filter settings. remove 50Hz and (if sampleRate>200Hz) 100Hz 
            opt.mains.do=1;
            
            % DOWNSAMPLING
            opt.downsample.do=0;
            
            % IDENTIFYING BAD SEGMENTS
            opt.bad_segments.do=0;
            
            % Set to 0 for now
            opt.africa.do=0;
            opt.epoch.do=0;
            opt.outliers.do=0;
            opt.coreg.do=0;
            
            %%%%%%%%%%%%%%%%%%%%%
            opt = osl_run_opt(opt); % this will save the file fffspmeeg.mat (prefix f for each filtering operation: higpass, notch 50Hz, notch 100Hz) 
            
            fprintf('Highpass done\n')
        end
        
        %% Phase 2 - downsample + bad segment
        if do_downs
            opt2=[];
            %             opt2.maxfilter.do=0;
            
            opt2.spm_files = opt.results.spm_files; % this should be fullfile(outputDir,'highpass_0.5.opt','fffspmeeg.mat')
            opt2.datatype='ctf';
            
            % optional inputs
            opt2.dirname=opt.dirname; % directory opt settings and results will be stored; fullfile(outputDir,'highpass_0.5.opt')
            %             opt2.convert.szpm_files_basenames = opt.results.spm_files_basenames;
            
            % DOWNSAMPLING
            opt2.downsample.do=1;
            %             opt2.downsample.freq=is.SampleRate/is.smoothFact;
            opt2.downsample.freq = downsample;
            
            % IDENTIFYING BAD SEGMENTS
            opt2.bad_segments.do=1;
            
            % Set to 0 for now
            opt2.africa.do=0;
            opt2.epoch.do=0;
            opt2.outliers.do=0;
            opt2.coreg.do=0;
            opt2.highpass.do=0;
            opt2.mains.do=0;
            %%%%%%%%%%%%%%%%%%%%%
            opt2 = osl_run_opt(opt2); % this saves the file Bdffspmeeg.mat (prefix d for downsampling and B forr Bad segments. )
        end
        
        %% phase 3 - ICA
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if do_ica
            opt3 = [];
            % required inputs
            opt3.spm_files = opt2.results.spm_files;
            opt3.datatype='ctf';
            
            % optional inputs
            opt3.dirname=opt2.dirname; % directory opt settings and results will be stored
            
            % africa settings
            opt3.africa.do=1;
            opt3.africa.todo.ident='auto';
%             opt3.africa.ident.artefact_chans    = {'EOG1','EOG2','EOG3'};
            opt3.africa.precompute_topos = false;
            %             opt3.africa.ident.mains_kurt_thresh = 0.5;
            opt3.africa.ident.max_num_artefact_comps = 10; % the default is 10
            opt3.africa.ident.do_kurt = true; % previous it is TRUE!
            opt3.africa.ident.do_cardiac = true;
            opt3.africa.ident.do_plots = true;
            opt3.africa.ident.do_mains = false;
            
            % turn the remaining options off
            %             opt3.maxfilter.do=0;
            %             opt3.convert.spm_files_basenames = opt2.results.spm_files_basenames;
            opt3.downsample.do=0;
            opt3.highpass.do=0;
            opt3.bad_segments.do=0;
            opt3.epoch.do=0;
            opt3.outliers.do=0;
            opt3.coreg.do=0;
            
            opt3=osl_check_opt(opt3);
            
            opt3 = osl_run_opt(opt3);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %% Phase 4 - Epoch + outliter + coreg
        if do_epoch
            opt4 = [];
            % required inputs
            opt4.spm_files = opt3.results.spm_files;
            opt4.datatype='ctf';
            
            % optional inputs
            opt4.dirname=opt3.dirname; % directory opt settings and results will be stored
            
            %% Epoching settings - different for rest and task 
            
            if task_phase
                opt4.epoch.do=1;
                opt4.epoch.time_range = [-0.5 1]; % stim onset, epoch end in secs
                
                states=1:7; % currently this is the position code, not OVC
                
                for istim=1:length(states)
                    label=['pos',num2str(states(istim))];
                    opt4.epoch.trialdef(istim).conditionlabel = label;
                    opt4.epoch.trialdef(istim).eventtype = num2str(states(istim));
                    opt4.epoch.trialdef(istim).eventvalue = [];
                end
                
                opt4.bad_segments.do=0;
                opt4.outliers.do=1;
                
            elseif rest_phase % Alon: not sure these are the right settings for rest..
                opt4.epoch.do=0;
                opt4.outliers.do=0;
                opt4.bad_segments.do=1;
            end
            
            %% coreg for subsequent source analysis - Already Done!
            opt4.coreg.do=0;
            opt4.coreg.use_rhino=0; % unless have head point data?
            opt4.coreg.useheadshape=0;
            
            opt4.downsample.do=0;
            opt4.africa.do=0;
            opt4.africa.todo.ica=0;
            opt4.africa.todo.ident=0;
            opt4.africa.todo.remove=0;

            opt4=osl_run_opt(opt4);
        end
        
    end
    if task_phase
        % merge all runs to a single epoched MEEG objects        
        cd(subjOutputDir)
        for iRun=1:length(runsDirsNames)
            fName = strcat(fullfile(root,'derivatives','preproc',sub),'/',runsDirsNames(iRun).name,'/',['highpass_' num2str(highpassCutoff) '.opt'],'/','ReABdffspmeeg.mat');
            toMerge.D{iRun} = spm_eeg_load(fName);
        end
        D = spm_eeg_merge(toMerge);
        
        % plot trials
        figure('units','normalized','outerposition',[0 0 0.6 0.3]);
        imagesc(D.time,[],squeeze(mean(D(:,:,:),3)));
        xlabel('Time (seconds)','FontSize',20);
        ylabel('Sensors','FontSize',20);colorbar
        title(sprintf('ERF %s',sub),'FontSize',20)
        set(gca,'FontSize',20)
        set(gca,'XLim',[-0.5 1])
    end
    
end