
%%

for iSj = is.whichSubj
    
    spN = [is.rawMEG is.fnSID_orig{iSj}  ];   % raw data path
    spL = [is.OPTPath is.fnSID_bids{iSj} ];   % output path
    
    
    for iR = 1:length(is.MEGruns_orig{iSj})
        ds_folder = fullfile(spN,[is.MEGruns_orig{iSj}{iR},'.ds']); % raw data of run
        localPath = fullfile(spL,is.MEGruns_bids{iSj}{iR}); % output path of run. Delete '.ds' from the end
        mkdir(localPath);
        
        %% Import
        
        
        S2 = [];
        S2.outfile = fullfile(localPath,'spmeeg.mat');
        %                 % currently in the plot data there are no eye-movement
        %                 % channels
        %                 S2.other_channels = {'UADC001','UADC002','UADC003'}; %
        
        D = osl_import(ds_folder,S2); % this will save the files spmeeg.mat and spmeeg.dat
        
        %% add up other channels
        %             D = D.chantype(find(strcmp(D.chanlabels,'UADC001')),'EOG1');
        %             D = D.chantype(find(strcmp(D.chanlabels,'UADC002')),'EOG2');
        %             D = D.chantype(find(strcmp(D.chanlabels,'UADC003')),'EOG3');
        %
        % D.save()
        %
        
%         %% Crop unused data - IMPORTANT!!!
%         S = struct;
%         S.D = fullfile(localPath,'spmeeg.mat');
%         S.prefix='';
%         event = ft_read_event(ds_folder);
%         sampleLastEvent = [event(length(event)).sample]';
%         winPostLastEvent = 500; % how long in ms to retain after the last trial trigger
%         S.timewin = [0,ceil(sampleLastEvent/is.sampleRate*1000) + winPostLastEvent];
%         
%         D=spm_eeg_crop_YL(S);
%         D.save()
%         
%         fprintf('data import done\n')
        
        %% OPT Preprocessing Pipeline!
        
        %% Phase 1 - Filter
        
        opt=[];
        
        spm_files{1}=fullfile(localPath,'spmeeg.mat');
        structural_files{1}=[]; % leave empty if no .nii structural file available
        
        opt.spm_files=spm_files;
        opt.datatype='ctf';
        
        % HIGHPASS
        if is.highpass ==0
            opt.highpass.do=0;
            opt.dirname=fullfile(localPath,['highpass_',num2str(is.highpass)]);
        else
            opt.highpass.cutoff = is.highpass; % create different folder for different frenquency band (0.5; 1; 20)
            opt.dirname=fullfile(localPath,['highpass_',num2str(opt.highpass.cutoff)]);
            opt.highpass.do=1;
        end
        
        % Notch filter settings. remove 50Hz and (if sampleRate>200Hz) 100Hz
        opt.mains.do=1;
        
        % DOWNSAMPLING
        opt.downsample.do=0;
        
        % IDENTIFYING BAD SEGMENTS
        opt.bad_segments.do=0;
        
        % Set to 0 for now
        opt.africa.do=0;
        opt.epoch.do=0;
        opt.outliers.do=0;
        opt.coreg.do=0;
        
        %%%%%%%%%%%%%%%%%%%%%
        opt = osl_run_opt(opt); % this will save the file fffspmeeg.mat (prefix f for each filtering operation: higpass, notch 50Hz, notch 100Hz)
        
        fprintf('Highpass done\n')
        
        %% Phase 2 - downsample + bad segment
        opt2=[];
        
        opt2.spm_files = opt.results.spm_files; % this should be fullfile(outputDir,'highpass_0.5.opt','fffspmeeg.mat')
        opt2.datatype='ctf';
        
        % optional inputs
        opt2.dirname=opt.dirname; % directory opt settings and results will be stored; fullfile(outputDir,'highpass_0.5.opt')
        
        % DOWNSAMPLING
        opt2.downsample.do=1;
        %             opt2.downsample.freq=is.SampleRate/is.smoothFact;
        opt2.downsample.freq = is.sampleRate/is.smoothFact; % currently 100Hz
        
        % IDENTIFYING BAD SEGMENTS
        opt2.bad_segments.do=1;
        
        % Set to 0 for now
        opt2.africa.do=0;
        opt2.epoch.do=0;
        opt2.outliers.do=0;
        opt2.coreg.do=0;
        opt2.highpass.do=0;
        opt2.mains.do=0;
        %%%%%%%%%%%%%%%%%%%%%
        opt2 = osl_run_opt(opt2); % this saves the file Bdffspmeeg.mat (prefix d for downsampling and B forr Bad segments. )
        
        %% phase 3 - ICA
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        opt3 = [];
        % required inputs
        opt3.spm_files = opt2.results.spm_files;
        opt3.datatype='ctf';
        
        % optional inputs
        opt3.dirname=opt2.dirname; % directory opt settings and results will be stored
        
        % africa settings
        opt3.africa.do=1;
        opt3.africa.todo.ident='auto';
        
        %             opt3.africa.ident.artefact_chans    = {'EOG1','EOG2','EOG3'};
        opt3.africa.precompute_topos = false;
        %                 opt3.africa.ident.mains_kurt_thresh = 0.5;
        %                 opt3.africa.ident.max_num_artefact_comps = 10; % the default is 10
        opt3.africa.ident.do_kurt = true; % previous it is TRUE!
        opt3.africa.ident.do_cardiac = true;
        opt3.africa.ident.do_plots = true;
        opt3.africa.ident.do_mains = false;
        
        % turn the remaining options off
        opt3.downsample.do=0;
        opt3.highpass.do=0;
        opt3.bad_segments.do=0;
        opt3.epoch.do=0;
        opt3.outliers.do=0;
        opt3.coreg.do=0;
        
        opt3=osl_check_opt(opt3);
        
        opt3 = osl_run_opt(opt3);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %% Phase 4 - Epoch + outliter + coreg
        opt4 = [];
        % required inputs
        opt4.spm_files = opt3.results.spm_files;
        opt4.datatype='ctf';
        
        % optional inputs
        opt4.dirname=opt3.dirname; % directory opt settings and results will be stored
        
        %% Epoching settings - different for rest and task
        
        
        if contains(is.MEGruns_bids{iSj}{iR},'task-rest')
            opt4.epoch.do=0;
            opt4.outliers.do=0;
            opt4.bad_segments.do=1;
            
        elseif contains(is.MEGruns_bids{iSj}{iR},'task-ovc') % task phase
            
            opt4.epoch.do=1;
            opt4.epoch.time_range = is.epoch_win_s;
            
            states=1:7; % currently this is the position code, not OVC
            
            for istim=1:length(states)
                label=['pos',num2str(states(istim))];
                opt4.epoch.trialdef(istim).conditionlabel = label;
                opt4.epoch.trialdef(istim).eventtype = num2str(states(istim));
                opt4.epoch.trialdef(istim).eventvalue = [];
            end
            
            opt4.bad_segments.do=0;
            opt4.outliers.do=1;
        end
        
        %% coreg for subsequent source analysis - Already Done!
        opt4.coreg.do=0;
        opt4.coreg.use_rhino=0; % unless have head point data?
        opt4.coreg.useheadshape=0;
        
        % turn the remaining options off
        
        opt4.downsample.do=0;
        opt4.africa.do=0;
        opt4.africa.todo.ica=0;
        opt4.africa.todo.ident=0;
        opt4.africa.todo.remove=0;
        
        opt4=osl_run_opt(opt4);
        
        %% Display Results
        opt4 = osl_load_opt(opt4.dirname);
        close all;
        fclose('all');
        
        
        
    end
    %%
    % merge all task phase runs to a single epoched MEEG objects
    cd(spL)
    taskRunsInds = find(contains(is.MEGruns_bids{iSj},'task-ovc'));
    for iTaskR=1:length(taskRunsInds)
        fName = strcat(fullfile(spL,is.MEGruns_bids{iSj}{taskRunsInds(iTaskR)}),'/',['highpass_' num2str(is.highpass) '.opt'],'/','ReABdffspmeeg.mat');
        toMerge.D{iTaskR} = spm_eeg_load(fName);
    end
    D = spm_eeg_merge(toMerge);
    
    % plot trials
    figure('units','normalized','outerposition',[0 0 0.6 0.3]);
    imagesc(D.time,[],squeeze(mean(D(:,:,:),3)));
    xlabel('Time (seconds)','FontSize',20);
    ylabel('Sensors','FontSize',20);colorbar
    title(sprintf('ERF %s',is.fnSID_bids{iSj}),'FontSize',20)
    set(gca,'FontSize',20)
    set(gca,'XLim',[-0.5 1])
end