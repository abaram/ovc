%% Wrapper script
%   
%   Key paths and params set in initialize_AB
%   Thisscript closely follows the scripts from Matthew Nour, based on
%   scripts by Yunzhe Liu. 


%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% [0] INITIALIZE
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
clear all
close all 
clc
restoredefaultpath

if exist('/Users/abaram/OneDrive - Nexus365','dir') % WASABI (FMRIB Mac)
    based = '/Users/abaram/OneDrive - Nexus365/projects/ovc/analysis/AB';
elseif exist('/Users/neurotheory/OneDrive - Nexus365','dir') % laptop
    based = '/Users/neurotheory/OneDrive - Nexus365/projects/ovc/analysis/AB';
end

addpath(genpath(based))
initialize_AB

is.whichSubj = 1:is.nSubj;

jobs = struct;
jobs.preproc = 0;
jobs.TDLM = 1;
jobs.summarise = 0;

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% [1] PREPROCESSING
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

if jobs.preproc
    Preprocessing_OPT_AB
end


%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% [2]  SEQUENCENESS ANALYSIS PIPELINE
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

if jobs.TDLM
    trainDecoders
end


%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% [3]  PLOT GROUP RESULTS 
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

if jobs.summarise
    summarise_and_plot_AB
end
