clear all
close all 
clc
restoredefaultpath

if exist('/Users/abaram/OneDrive - Nexus365','dir') % WASABI (FMRIB Mac)
    based = '/Users/abaram/OneDrive - Nexus365/projects/ovc/analysis/AB';
elseif exist('/Users/neurotheory/OneDrive - Nexus365','dir') % laptop
    based = '/Users/neurotheory/OneDrive - Nexus365/projects/ovc/analysis/AB';
end

addpath(genpath(based))
initialize_AB

cd(is.rootMEG)

is.whichSubj = 1:is.nSubj;

codes = {'stimId','ovc'};

for iSj = is.whichSubj
    for iCode=1:length(codes)        
        load(fullfile(is.rootMEG,'outputTDLM',['trained_decoders_' codes{iCode}],[is.fnSID_bids{iSj} '_stateDecoder.mat']))
        plotDecodingAccuracy(is.fnSID_bids{iSj},codes{iCode},whenInMS,stateDecoderProb,stimlabel)
    end
    
end