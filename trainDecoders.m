%% Train decoders - stim code

% addpath(genpath(is.spm_path));
% whenInMS = is.tss*is.msPerSample;
whenInMS = is.whichTimes*is.msPerSample;
nRuns = 24; % Task + Rest runs, max across subjects

codes = {'md'}; %{'stimId','ovc','md'};
nStates = 2;% [14,7,2];

% % is.whichTimes used to be the indices of is.tss time bins to be used for
% % decoder training.
% is.whichTimes = 1:length(is.tss);

goodChannel=nan(is.nSubj,is.numChan, nRuns);    % nsubject*nsensors*nruns
num_trial = nan(is.nSubj, nRuns);               % new variable to avoid hard coding
num_tpts = nan(is.nSubj, nRuns);                % new variable to avoid hard coding
% saveWrap = @(gnf, whenInMS, iSj, is, codeName) save([is.OutputDir 'trained_decoders_' codeName '/' is.fnSID_bids{iSj}], 'gnf', 'whenInMS', 'is','stimlabel','iSj'); % save by subject number
saveWrapAB = @(gnf,trueDataScaledAll,stateDecoderProb, stimlabel, whenInMS, iSj, is, codeName) save(fullfile(is.OutputDir,['trained_decoders_' codeName] , is.fnSID_bids{iSj}), 'gnf', 'trueDataScaledAll','stateDecoderProb', 'stimlabel', 'whenInMS', 'iSj','is'); % save by subject number


%% Good Channels
for iSj = is.whichSubj  % index of the subj w.r.t. canonical subject list
    
    %   AB, accounting for a subject that didn't complete; all runs.
    all_runs = 1:length(is.MEGruns_bids{iSj});
    for iRun = 1:length(all_runs)
       
        tempdir = [is.OPTPath is.fnSID_bids{iSj} filesep is.MEGruns_bids{iSj}{iRun} '/highpass_' num2str(is.highpass) '.opt'];        
        load(fullfile(tempdir, 'opt.mat'));
        
        if isempty (opt.results.spm_files_epoched{1,1}) % rest runs - no epoched data
            sessionMEG=spm_eeg_load(fullfile(tempdir,[opt.results.spm_files_basenames{1,1}]));
        else % task runs
            sessionMEG=spm_eeg_load(fullfile(tempdir,[opt.results.spm_files_epoched_basenames{1,1}]));
        end
        
        goodChan = indchantype(sessionMEG,'meeg','GOOD');
        allChan  = indchantype(sessionMEG,'meeg','ALL');
        [~,indBad_inAllChan]=setdiff(allChan,goodChan); % indBad is the index in allChanInd of the bad channels (currently it's [badChannelNumber - 1], because channel #1 is not meeg channel)
        goodChan_inAllChan=ones(1,is.numChan);
        goodChan_inAllChan(indBad_inAllChan)=0;
        goodChannel(iSj,:,iRun)=goodChan_inAllChan; % goodchannel has 0s in the indexes of the bad channels in allChan, not in the chanel number (currently it's [badChannelNumber - 1], because channel #1 is not meeg channel)
        clear opt;
        
        % MMN we want to keep track of the following variables to avoid hard-coding below
        num_trial(iSj, iRun) = size(sessionMEG, 3);
        num_tpts(iSj, iRun) = size(sessionMEG, 2);
        
    end
end

%%
%parfor iSj=1:length(is.whichSubj)
for iSj = is.whichSubj
    
    % find non-rest runs
    taskRunsInd = find(contains(is.MEGruns_bids{iSj},'task-ovc'));
    
    % get the behavioural variables to be in the same shape as the MEG data
    % (accounting for blocks in the pilot being split into 2, and for the
    % last runs not to hav run in sub-01)
    % I need this because te triggers currently don't signal OVCs, only
    % position
    Btmp = load(fullfile(is.rootMEG, 'behData', is.fnSID_bids{iSj},'scan_1.mat'));
    B.stimId.all = reshape(Btmp.D.walk.stimId,[200,16]); B.stimId.all = B.stimId.all(:,1:length(taskRunsInd));
    B.pos.all = reshape(Btmp.D.walk.pos,[200,16]); B.pos.all = B.pos.all(:,1:length(taskRunsInd));
    B.ovc.all = reshape(Btmp.D.walk.ovc,[200,16]); B.ovc.all = B.ovc.all(:,1:length(taskRunsInd));
    B.ovc.all = B.ovc.all + 4; % move from -3:3 range to 1:7 range
    B.md.all = reshape(Btmp.D.walk.md,[200,16]); B.md.all = B.md.all(:,1:length(taskRunsInd));
    
    
    
    %% train stim decoders
    
    temp_num_tpts = min(num_tpts(iSj, :));
    temp_num_trials = max(num_trial(iSj, :));
    data= nan(is.numChan, temp_num_tpts, temp_num_trials, length(taskRunsInd));  % [channel, epoch_time_points, trials, sessions]
    trialindex = zeros(temp_num_trials, length(taskRunsInd));
    
    % YL
    %data= nan(273,401,96,length(lciInd)); % channel*time points(-3s - 1s)*trials*sessions
    %trialindex=zeros(96,length(lciInd));
    
    %%
    % Getting the data
    for iR = 1:length(taskRunsInd)
        iR_inAllRuns = taskRunsInd(iR);
        % load the epoched data
        dir_temp = [is.OPTPath is.fnSID_bids{iSj} filesep is.MEGruns_bids{iSj}{taskRunsInd(iR)} '/highpass_' num2str(is.highpass) '.opt'];
        %dir_temp=[is.OPTPath strrep(is.fnDate{iSj},'-','_') '\' is.fnMEG{iSj} '_' num2str(lciInd(ii),'%02d') '.ds\highpass_' num2str(is.highpass) '.opt'];
        
        opt=load(fullfile(dir_temp, 'opt.mat'));
        opt=opt.opt;
        DLCI=spm_eeg_load(fullfile(dir_temp,[opt.results.spm_files_epoched_basenames{1,1}]));
        
        % load all MEG channels:
        chan_MEG = indchantype(DLCI,'meeg');
        
        % load good trials
        allconds=DLCI.condlist;       
        all_trls = sort([DLCI.indtrial(allconds(:))]);
        goodTrls = sort([DLCI.indtrial(allconds(:),'GOOD')]);
        goodTrls_ind=ismember(all_trls,goodTrls);
        
        trialindex(goodTrls_ind,iR)=1; % if the run was missing some trials, they will have a 0
        
        % get cleanning data.
        % AB: this is still with ALL meeg channels, not just good channels
        cldata = DLCI(chan_MEG,:,goodTrls);
        data(:,:,goodTrls_ind,iR)= cldata;
        
        %         condition_log = [condition_log; DLCI.conditions'];
        %         B.ovc.good(good_ind,iR) = B.ovc.all(good_ind,iR); % B.ovc.good has NaNs in Bad trials or trials after the last MEG trial
        %         B.stimId.good(good_ind,iR) = B.stimId.all(good_ind,iR); % B.ovc.good has NaNs in Bad trials or trials after the last MEG trial
        %         B.pos.good(good_ind,iR) = B.pos.all(good_ind,iR); % B.ovc.good has NaNs in Bad trials or trials after the last MEG trial
    end
    
    % reshape;
    % **** AB: this combines data from all runs, not just three as
    % in the comment below! ****
    Cleandata=reshape(data,[size(data,1),size(data,2),size(data,3)*size(data,4)]); % nAllMeegChannels x nTimepointsInEpoch x nTotalTrials
    goodTrialsInd=reshape(trialindex,[size(trialindex,1)*size(trialindex,2),1]);  % nTotalTrials x 1
    Cleandata_subset=Cleandata(:,:,logical(goodTrialsInd));                 % good trials (NB. my FL doesn't permit RT and has no 'upside down' trials, as YL)
    
    %%
        
    % if use eyeblink, get rid of channels influenced by eyeblink, mostly in
    % the frontal area, Otherwise, subset channels that are useable for both ALL runs
    if is.eyeBlinkSens==1
        %commonmisschannel=any(any(isnan(Cleandata_subset),3),2);
        goodData = Cleandata_subset(~eyeBlinkSens,:,:);
    else
        sjchannel=squeeze(goodChannel(iSj,:,:));
        goodchannindex= sum(sjchannel,2,'omitnan')==length(is.MEGruns_bids{iSj});                         % YL
        
        %lcidata(any(any(isnan(lcidata),3),2),:,:) = [];
        goodData = Cleandata_subset(logical(goodchannindex),:,:); %nGoodChannels x nTimepointsInEpoch x nGoodTrials
    end
    
    % if doing further artifect correction - Zeb's approach
    %     if is.Zebcorrection ==1
    %         for i=1:size(lcidata,3)
    %             [lcidata_new(:,:,i), nRej] = artToZero(squeeze(lcidata(:,:,i))');
    %             lcidata_correction(:,:,i)=lcidata_new(:,:,i)';
    %             %disp(['sj' num2str(iSj) ': trial=' num2str(i) ' (after Preprocessing) further rejected ' num2str(100*nRej/prod(size(lcidata(:,:,i)))) '% of data as artifact']);
    %         end
    %         lcidata = lcidata_correction;
    %         clear lcidata_new;
    %         clear lcidata_correction;
    %     end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%
    
    
    for iCode = 1:length(codes)
        tic
        clear labStim labStim_subset stimlabel gnf labels
        % get labels (according to current code), only of GOOD trials
        labStim = B.(codes{iCode}).all(:);
        labStim_subset = labStim(logical(goodTrialsInd));
        stimlabel=labStim_subset;
        
        % creates gnf, which is a cell array of structs, with dimensions of nGoodTrials *
        % trainTime * BStates  (AB: YL also looped over L1 normalisation
        % parameter, I don't)
        %             gnf = cell(1, length(is.tss), nStates(iCode), length(is.ENL1));
        gnf = cell(size(goodData,3), length(is.whichTimes), nStates(iCode)); % nTrials x nTimePoints x nStates
        
        
        stateDecoderProb = nan(nStates(iCode),length(is.whichTimes),size(goodData,3)); % states x timePoints x nGoodTrials
        betas = nan(nStates(iCode),length(is.whichTimes),size(goodData,3)); % states x timePoints x nGoodTrials
        intercepts = nan(nStates(iCode),length(is.whichTimes),size(goodData,3)); % states x timePoints x nGoodTrials
        trueDataScaledAll = nan(size(goodData,1), length(is.whichTimes), size(goodData,3)); % nGoodChannels x nTimePoints x nGoodTrials
        
        % Prepare Dataset for training classifer. AB: no null data for now
        % as we're only looking at decoding. 
        for iTT = 1:length(is.whichTimes) % which specific time to be trained on 200 ms & 400 ms
%             nullData=[];
            trueData=[];
            % subtract ISI to get a baseline (for lex stims)
            for iTr=1:size(goodData,3) % AB: loop over good trials
                % AB currently don't need null data - it's only used to
                % reduce spatial correlations betwen models which is
                % important for sequenceness. Currently we just want to
                % know if we can decode.    
                
                
                % MMN index times w.r.t. pic-onset (more general)
                %             nullData(:,iTr) = squeeze( lcidata(:, is.picOnsetSlice - is.nullslice/is.msPerSample,iTr));  % MMN nullslice - there is no equivalent for YL slice owing to differences in task structure. I fixed this value at -500 ms to lie at a random point within the ITI (random owing to jittered ITI), uncorrupted by motor artefact
                %             trueData(:,iTr) = squeeze( lcidata(:, is.picOnsetSlice + is.tss(is.whichTimes(iTT)), iTr));
                trueData(:,iTr) = squeeze( goodData(:, is.picOnsetSlice + is.whichTimes(iTT), iTr));
                
                %nullData(:,ii) = squeeze(lcidata(:,301-ceil(isis(ii)/is.msPerSample),ii)); % 301 is the onset of lex stimuli in the -3s-1s trials
                %trueData(:,ii) = squeeze(lcidata(:,301+is.tss(is.whichTimes(iTT)),ii));
            end
            
            %nullData' = nullData' - nullData'*sDst;  % spatial highpass
            %trueData' = trueData' - trueData'*sDst;  % spatial highpass
            
            % May need to turn it off for time lagged regression
            %         nullData = scaleFunc(nullData');  % note, there are other options for how to scale the data
            trueData = scaleFunc(trueData'); 
            
            %AB
            trueDataScaledAll(:,iTT,:) = trueData';
            
            
            % AB Set up the classifers, leave one out cross validation
            for iState=1:nStates(iCode)   % train classifiers on iT sample of data.
                %disp(['sj' num2str(iSj) ', iShuf=' num2str(iShuf) ', iT=' num2str(is.whichTimes(iTT)) ', iC=' num2str(iC)])
                
                %                 labels = [stimlabel == iC; zeros(size(nullData,1),1)];
                labels = stimlabel == iState;
                %                 if iShuf==1
                %                     labels = labels;
                %                 else
                %                     labels = labels(randperm(length(labels)));
                %                 end
                
                for iTr = 1:size(goodData,3) % loop over trials for leave one trial out  cross validation
                    iL1 =1; % in case want to loop over regularisation parameters
                    l1p = is.ENL1(iL1); l2p = 0; % alpha = L1/(2*L2+L1) ; lambda = 2*L2+L1
                    %                         [beta, fitInfo] = lassoglm([trueData; nullData], labels, 'binomial', ...
                    %                             'Alpha', l1p / (2*l2p+l1p), 'Lambda', 2*l2p + l1p, 'Standardize', false);
                    
                    [beta, fitInfo] = lassoglm(trueData(1:size(trueData,1)~=iTr,:), labels(1:size(trueData,1)~=iTr), 'binomial', ...
                        'Alpha', l1p / (2*l2p+l1p), 'Lambda', 2*l2p + l1p, 'Standardize', false);
                    
                    stateDecoderProb(iState,iTT,iTr) = 1 / (1+exp( - ( fitInfo.Intercept + beta'*trueData(iTr,:)' )));
                    
                    gnf{iTr,iTT,iState}.beta = beta; gnf{iTr,iTT,iState}.Intercept = fitInfo.Intercept;
                    disp(['sj' num2str(iSj) ', ' codes{iCode} ' code, state' num2str(iState) ': X-val trial ' num2str(iTr)  ' out of ' num2str(sum(goodTrialsInd)) ' Good trials, time point ' num2str(is.whichTimes(iTT)*is.msPerSample) ' ms'] );                    
                end

            end
%             saveWrap(gnf, whenInMS, iSj,stimlabel);  % anonymous function to call 'save' in parfor
            
        end
        toc
        saveWrapAB(gnf, trueDataScaledAll, stateDecoderProb, stimlabel, whenInMS, iSj, is, codes{iCode})%         saveWrap(gnf, whenInMS, iSj, is, codes{iCode});  % MMN (retain is)        
        plotDecodingAccuracy(is.fnSID_bids{iSj},codes{iCode},whenInMS,stateDecoderProb,stimlabel)
    end
    toc;
end

