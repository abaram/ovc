rootData = '/Users/abaram/OneDrive - Nexus365/projects/megOvc/pilotBeijing/behData';
subjects = {'sub-02','sub-03','sub-04','sub-10'};
sess = 'scan';

% plot percent correct in rew and dir for each subject
rewCorrect = nan(length(subjects),8);
dirCorrect = nan(length(subjects),8);
for iSubj = 1:length(subjects)
    rootDataSubj = fullfile(rootData,subjects{iSubj});
    files = dir(fullfile(rootDataSubj,[sess '*']));
    for iFile = 1:length(files)
        fprintf('\n%s, %s \n', subjects{iSubj}, files(iFile).name)
        load(fullfile(rootDataSubj,files(iFile).name),'D')
        nBlks = length(D.walk.T);
        for iBl=1:nBlks
            if ~isnan(mean(D.walk.C.rew.correct{iBl}))
                rewCorrect(iSubj,iBl) = mean(D.walk.C.rew.correct{iBl});
            end
            if ~isnan(mean(D.walk.C.dir.correct{iBl}))
                dirCorrect(iSubj,iBl) = mean(D.walk.C.dir.correct{iBl});
            end
            fprintf('%0.2f \n', mean(D.walk.C.rew.correct{iBl}))
        end
    end
end

figure;
subplot(2,1,1)
title ('Reward catch trials % correct')
plot(rewCorrect','-*'); legend(subjects,'Location','SouthEast')
subplot(2,1,2)
title ('Dir catch trials % correct')
plot(dirCorrect','-*'); 