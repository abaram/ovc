% subject list to preprocesess

is.nSubj = 1;

%% PATIENTS
%% [1]
is.fnSID{is.nSubj} = 's107';
is.fnMEG{is.nSubj} = 'MG05756_SZReplay_20190511_'; 
is.MEGruns{is.nSubj} = {'' 'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;

%% [2]
is.fnSID{is.nSubj} = 's109';
is.fnMEG{is.nSubj} = 'MG05757_SZReplay_20190512_'; 
is.MEGruns{is.nSubj} = {'' 'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [3]
is.fnSID{is.nSubj} = 's113';
is.fnMEG{is.nSubj} = 'MG05766_SZReplay_20190516_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [4]
is.fnSID{is.nSubj} = 's111';
is.fnMEG{is.nSubj} = 'MG05767_SZReplay_20190518_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [5]
is.fnSID{is.nSubj} = 's112';
is.fnMEG{is.nSubj} = 'MG05769_SZReplay_20190519_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [6]
is.fnSID{is.nSubj} = 's110';
is.fnMEG{is.nSubj} = 'MG05787_SZReplay_20190530_'; 
is.MEGruns{is.nSubj} = {'' 'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [7]
is.fnSID{is.nSubj} = 's115';
is.fnMEG{is.nSubj} = 'MG05793_SZReplay_20190602_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [8]
is.fnSID{is.nSubj} = 's105';
is.fnMEG{is.nSubj} = 'MG05815_SZReplay_20190615_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [9]
is.fnSID{is.nSubj} = 's102';
is.fnMEG{is.nSubj} = 'MG05816_SZReplay_20190616_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [10]
is.fnSID{is.nSubj} = 's117';
is.fnMEG{is.nSubj} = 'MG05827_RLReplay_20190620_'; % correct, despote 'RL' name 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [11]
is.fnSID{is.nSubj} = 's118';
is.fnMEG{is.nSubj} = 'MG05832_SZReplay_20190623_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [12]
is.fnSID{is.nSubj} = 's103';
is.fnMEG{is.nSubj} = 'MG05845_SZReplay_20190629_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [13]
is.fnSID{is.nSubj} = 's119';
is.fnMEG{is.nSubj} = 'MG05848_SZReplay_20190630_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [14]
is.fnSID{is.nSubj} = 's120';
is.fnMEG{is.nSubj} = 'MG05852_SZReplay_20190703_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [15]
is.fnSID{is.nSubj} = 's101';
is.fnMEG{is.nSubj} = 'MG05873_SZReplay_20190711_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [16]
is.fnSID{is.nSubj} = 's121';
is.fnMEG{is.nSubj} = 'MG05940_SZReplay_20190819_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [17]
is.fnSID{is.nSubj} = 's124';
is.fnMEG{is.nSubj} = 'MG05968_SZReplay_20190918_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [18]
is.fnSID{is.nSubj} = 's125';
is.fnMEG{is.nSubj} = 'MG05793_SZReplay_20190921_'; 
is.MEGruns{is.nSubj} = {'rest0_' '' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''}; %first sFL was aborted halfway through as lost Eyelink
% increment
is.nSubj = is.nSubj + 1;


%% [19]
is.fnSID{is.nSubj} = 's126';
is.fnMEG{is.nSubj} = 'MG05974_SZReplay_20190922_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''}; %4th AL as major (15 min) break after , due to technical fault. So wanted Rest1_to be fresh after AL
% increment
is.nSubj = is.nSubj + 1;


%% [20]
is.fnSID{is.nSubj} = 's114';
is.fnMEG{is.nSubj} = 'MG05977_SZReplay_20190923_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [21]
is.fnSID{is.nSubj} = 's127';
is.fnMEG{is.nSubj} = 'MG05979_SZReplay_20190925_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [22]
is.fnSID{is.nSubj} = 's128';
is.fnMEG{is.nSubj} = 'MG05987_SZReplay_20190930_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [23]
is.fnSID{is.nSubj} = 's104';
is.fnMEG{is.nSubj} = 'MG05992_SZReplay_20191002_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' '' ''}; % repeated  at end to see if improvement on chance could be achieved, but perf is same
% increment
is.nSubj = is.nSubj + 1;


%% [24]
is.fnSID{is.nSubj} = 's129';
is.fnMEG{is.nSubj} = 'MG05997_SZReplay_20191004_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [25]
is.fnSID{is.nSubj} = 's130';
is.fnMEG{is.nSubj} = 'MG05999_SZReplay_20191007_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [26]
is.fnSID{is.nSubj} = 's108';
is.fnMEG{is.nSubj} = 'MG06002_SZReplay_20191009_'; 
is.MEGruns{is.nSubj} = {'rest0_' '' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''}; %run 2 false start
% increment
is.nSubj = is.nSubj + 1;


%% [27]
is.fnSID{is.nSubj} = 's131';
is.fnMEG{is.nSubj} = 'MG06008_SZReplay_20191013_'; 
is.MEGruns{is.nSubj} = {'' 'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''}; %run 1 false start
% increment
is.nSubj = is.nSubj + 1;


%% [28]
is.fnSID{is.nSubj} = 's132';
is.fnMEG{is.nSubj} = 'MG06019_SZReplay_20191019_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''}; %run 1 false start
% increment
is.nSubj = is.nSubj + 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
is.numPat = is.nSubj - 1;
%% CONTROLS

%% [29]
is.fnSID{is.nSubj} = 's201';
is.fnMEG{is.nSubj} = 'MG05844_SZReplay_20190628_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [30]
is.fnSID{is.nSubj} = 's202';
is.fnMEG{is.nSubj} = 'MG05856_SZReplay_20190704_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [31]
is.fnSID{is.nSubj} = 's203';
is.fnMEG{is.nSubj} = 'MG05890_SZReplay_20190718_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [32]
is.fnSID{is.nSubj} = 's204';
is.fnMEG{is.nSubj} = 'MG05892_SZReplay_20190719_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [33]
is.fnSID{is.nSubj} = 's206';
is.fnMEG{is.nSubj} = 'MG05909_SZReplay_20190725_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' '' 'rest1_' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [34]
is.fnSID{is.nSubj} = 's205';
is.fnMEG{is.nSubj} = 'MG05898_SZReplay_20190722_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [35]
is.fnSID{is.nSubj} = 's207';
is.fnMEG{is.nSubj} = 'MG05914_SZReplay_20190729_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [36]
is.fnSID{is.nSubj} = 's208';
is.fnMEG{is.nSubj} = 'MG05939_SZReplay_20190815_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' '' 'rest1_' '' '' '' 'rest2_' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [37]
is.fnSID{is.nSubj} = 's209';
is.fnMEG{is.nSubj} = 'MG05941_SZReplay_20190821_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [38]
is.fnSID{is.nSubj} = 's210';
is.fnMEG{is.nSubj} = 'MG05944_SZReplay_20190823_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [39]
is.fnSID{is.nSubj} = 's211';
is.fnMEG{is.nSubj} = 'MG05947_SZReplay_20190828_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [40]
is.fnSID{is.nSubj} = 's212';
is.fnMEG{is.nSubj} = 'MG05949_SZReplay_20190829_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [41]
is.fnSID{is.nSubj} = 's213';
is.fnMEG{is.nSubj} = 'MG05952_SZReplay_20190902_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [42]
is.fnSID{is.nSubj} = 's214';
is.fnMEG{is.nSubj} = 'MG05955_SZReplay_20190905_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [43]
is.fnSID{is.nSubj} = 's215';
is.fnMEG{is.nSubj} = 'MG05963_SZReplay_20190916_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [44]
is.fnSID{is.nSubj} = 's217';
is.fnMEG{is.nSubj} = 'MG05971_SZReplay_20190919_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [45]
is.fnSID{is.nSubj} = 's218';
is.fnMEG{is.nSubj} = 'MG05985_SZReplay_20190930_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [46]
is.fnSID{is.nSubj} = 's219';
is.fnMEG{is.nSubj} = 'MG06005_SZReplay_20191011_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [47]
is.fnSID{is.nSubj} = 's220';
is.fnMEG{is.nSubj} = 'MG06006_SZReplay_20191012_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [48]
is.fnSID{is.nSubj} = 's216';
is.fnMEG{is.nSubj} = 'MG06007_SZReplay_20191012_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [49]
is.fnSID{is.nSubj} = 's221';
is.fnMEG{is.nSubj} = 'MG06010_SZReplay_20191014_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [50]
is.fnSID{is.nSubj} = 's222';
is.fnMEG{is.nSubj} = 'MG06018_SZReplay_20191018_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [51]
is.fnSID{is.nSubj} = 's223';
is.fnMEG{is.nSubj} = 'MG06020_SZReplay_20191021_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [52]
is.fnSID{is.nSubj} = 's224';
is.fnMEG{is.nSubj} = 'MG06023_SZReplay_20191023_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [53]
is.fnSID{is.nSubj} = 's225';
is.fnMEG{is.nSubj} = 'MG06027_SZReplay_20191025_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [54]
is.fnSID{is.nSubj} = 's226';
is.fnMEG{is.nSubj} = 'MG06051_SZReplay_20191112_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [55]
is.fnSID{is.nSubj} = 's227';
is.fnMEG{is.nSubj} = 'MG06059_SZReplay_20191115_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% increment
is.nSubj = is.nSubj + 1;


%% [56]
is.fnSID{is.nSubj} = 's228';
is.fnMEG{is.nSubj} = 'MG06060_SZReplay_20191116_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' '' ''}; % session 12 a dud
% increment
is.nSubj = is.nSubj + 1;


%% [57]
is.fnSID{is.nSubj} = 's229';
is.fnMEG{is.nSubj} = 'MG06076_SZReplay_20191123_'; 
is.MEGruns{is.nSubj} = {'rest0_' 'sFL1' 'sFL2' 'sFL3' 'sFL4' '' '' '' 'rest1_' '' 'rest2_' '' '' '' ''};
% no increment for final subj


