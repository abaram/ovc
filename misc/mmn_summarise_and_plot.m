close all
restoredefaultpath
addpath(genpath(based))

%% --------------------------------------------------------------------------
% Load pre-computed effects and define samples

cd(is.OutputDir)
PRE = load('StimAll_rst0');
POST1 = load('StimAll_rst1');
%POST2 = load('StimAll_rst2');

% define POST learning rest as 1st post-learning session (as per YL)
POST = POST1;

% define POST learning rest as mean of all post-learning sessions
%POST.sfAll = (POST1.sfAll + POST2.sfAll)/2;
%POST.sbAll = (POST1.sbAll + POST2.sbAll)/2;

% Final sample
ex = [22 32 47]; % rationale in paper
patId = setdiff(1:is.numPat, ex);
conId = setdiff(is.numPat+1:is.nSubj, ex);

samples = {};
samples{1,1} = conId;   samples{1,2} = 'CON ';  samples{1,3} = 'b';
samples{2,1} = patId;   samples{2,2} = 'PAT ';  samples{2,3} = 'r';

% FWD - BWD [SEQUENCENESS]
PRE_seq =  PRE.sfAll(2:end, :, :) -  PRE.sbAll(2:end, :, :); % 1st lag is 0ms (NaN)
POST_seq = POST.sfAll(2:end, :, :) - POST.sbAll(2:end, :, :);

%% --------------------------------------------------------------------------
% Sequenceness x lag

figure();
set(gcf, 'Units', 'point', 'Position', [50 50 700, 1000])
for isess = 1:2
    if isess == 1
        b = PRE_seq ; % [lag, perm, subj]
        tt = 'PRE-learning rest';
    else
        b =  POST_seq;
        tt = 'POST-learning rest';
    end
    
    for nS = 1:size(samples,1)
        
        subplot(2, 2, isess+2*(nS-1));
        
        mn = squeeze(mean(b(:, 1, samples{nS, 1}),3));
        sem = squeeze(std(b(:, 1, samples{nS, 1}), [], 3))./sqrt(length(samples{nS,1}));
        shadedErrorBar(10:10:600, mn, sem, samples{nS, 3}, 1);
        
        % for each permutation, calculate mean absolute sequenceness over participants,
        % then take the maximal value over all perms and lags
        hold on;
        thresh = max(max(abs(mean(b(:, 2:end, samples{nS,1}),3))));
        plot([0 600], [thresh, thresh], [samples{nS,3} ':'], 'LineWidth', 2)
        plot([0 600], -[thresh, thresh],  [samples{nS,3} ':'], 'LineWidth', 2)
        ylabel('sequenceness', 'FontSize', 16)
        title([samples{nS, 2} tt], 'FontSize', 16)
        ylim([-0.03 0.03])
        xlabel('lag (ms)', 'FontSize', 16)
    
    end
    
end

%% --------------------------------------------------------------------------
% Session x diagnosis ANOVA at temporal ROI from Liu et al, 2019

normalize_effect = 0;   % optional SD scaling across lags to minimise effect of outliers
lag = 5;                % time bin of temporal ROI (remember bin 1 = 0ms, so 40ms = '5')

if normalize_effect
    % PRE_seq is [lag, perm, subj]
    PRE_seq = normalize(PRE.sfAll(2:end, :, :) - PRE.sbAll(2:end, :, :), 1, 'scale');       % 1st lag is 0ms (NaN)
    POST_seq = normalize(POST.sfAll(2:end, :, :) - POST.sbAll(2:end, :, :), 1, 'scale');
else
    PRE_seq = PRE.sfAll(2:end, :, :) - PRE.sbAll(2:end, :, :); % 1st lag is 0ms (NaN)
    POST_seq = POST.sfAll(2:end, :, :) - POST.sbAll(2:end, :, :);
end

figure()
set(gcf, 'Units', 'point', 'Position', [800 750 300, 300])
d = {};
d{1} = permute([PRE_seq(lag, 1, conId), POST_seq(lag, 1, conId)], [3 2 1]) ;
d{2} = permute([PRE_seq(lag, 1, patId), POST_seq(lag, 1, patId)], [3 2 1]);
grouped_errorbar_scatter(d, {'PRE', 'POST'});
if normalize_effect, ylabel('sequenceness (SD scaled)', 'FontSize', 14), else, ylabel('sequenceness', 'FontSize', 14), end

session_by_diagnosis_ANOVA = simple_mixed_anova([d{1}; d{2}], ...
    [ones(length(conId),1); zeros(length(patId),1)], ...
    {'Diagnosis'}, {'Session'})


