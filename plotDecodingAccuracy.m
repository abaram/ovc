function plotDecodingAccuracy(subjName,codeName,whenInMS,stateDecoderProb,stimlabel)
% stateDecoderProb: nStates (depends on codeName)x nTimePoints x
% nGoodTrials (depends on subject)

chanceLevel = 1/size(stateDecoderProb,1);
accuracy = nan(size(stateDecoderProb,2),1); % nTimePoints x 1
[~, predictedState] = max(stateDecoderProb,[],1);
predictedState = squeeze(predictedState);
for iTT = 1:size(predictedState,1)
    accuracy(iTT) = sum(predictedState(iTT,:)'==stimlabel) / length(stimlabel);    
end


figure 
title(sprintf('%s, %s', subjName, codeName))
hold on
plot([whenInMS(1),whenInMS(end)],[chanceLevel,chanceLevel],'--')
plot(whenInMS',accuracy,'-r')
xlabel('time post stim(ms)')
ylabel('X-val accuracy')
xlim([whenInMS(1),whenInMS(end)])
